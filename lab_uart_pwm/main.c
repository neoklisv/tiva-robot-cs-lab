#include <stdint.h>
#include <stdbool.h>
#include <math.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/sysctl.h"
#include "driverlib/fpu.h"
#include "driverlib/gpio.h"
#include "driverlib/debug.h"
#include "driverlib/pwm.h"
#include "driverlib/pin_map.h"
#include "inc/hw_gpio.h"
#include "driverlib/uart.h"
#include "driverlib/qei.h"

// interrupt-timer definitions
#include "inc/tm4c1294ncpdt.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"



#define PWM_FREQUENCY 50000 //50KHz PWM Frequency
#define APP_PI 3.1415926535897932384626433832795f
#define STEPS 256


#define DownLimit 10 //Minimum PWM output value
#define UpLimit 90 //MAximum PWM output value
int Setpoint = 300; //101949=1999*51 define Setpoint for motor command (1:51 is the transmission ratio of the speed reducer).
 //With this command I expect a full rotation of the shaft. Tip:Command must be positive value for this code
//note: I have set it to 300 for home testing



int error=0; //error=Desired Angle(in Counts)- Position(counts-measured Value of Angle from encoder)
#define Kp 10 //define Kp gain of PD Controller 17
#define Kd 0.05 //define Kd gain

float P_term=0 ;
float D_term=0 ;
float u=0 ; //output command(%)


volatile int32_t Direction; //Direction of motor shaft
volatile int32_t Velocity; //Velocity of motor shaft in counts/period
volatile uint32_t Position; //Position of motor shaft in counts (360 degrees -> 1999 counts)
volatile int response[10000];
volatile int response_index=0;
int LastPosition=0;

void hbridge();
void softbridge();
void UARTsendint(int score);
volatile uint32_t ui32Load;


int main(void)
{


volatile uint32_t ui32BlueLevel;
volatile uint32_t ui32PWMClock;
volatile uint32_t ui32SysClkFreq;
volatile uint32_t ui32Index;
volatile uint32_t ui32Period;


//CPU clock setup
ui32SysClkFreq = SysCtlClockFreqSet((SYSCTL_XTAL_25MHZ | SYSCTL_OSC_MAIN |
SYSCTL_USE_PLL | SYSCTL_CFG_VCO_480), 120000000);


//ENABLING VARIOUS PERIPHERALS
SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOG);
SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM0);
SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);


//UART
SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
GPIOPinConfigure(GPIO_PA0_U0RX);
GPIOPinConfigure(GPIO_PA1_U0TX);
GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);
UARTConfigSetExpClk(UART0_BASE, ui32SysClkFreq, 115200,(UART_CONFIG_WLEN_8 |UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE));





//QEI SETUP
SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOL); // Enable the GPIOL peripheral for the input signals of the Encoder
SysCtlPeripheralEnable(SYSCTL_PERIPH_QEI0); // Enable the QEI0 peripheral
SysCtlDelay(10); //wait until it is ready
 /* Configure GPIO pins.*/
 GPIOPinConfigure(GPIO_PL1_PHA0); //Pin PC5 as PhA1
GPIOPinConfigure(GPIO_PL2_PHB0); //Pin PC6 as PhB1
 GPIOPinTypeQEI(GPIO_PORTL_BASE,GPIO_PIN_1 |GPIO_PIN_2); //Pins PC5 & PC6 for the QEI module
 QEIDisable(QEI0_BASE);
 QEIVelocityDisable(QEI0_BASE);
 QEIIntDisable(QEI0_BASE, (QEI_INTERROR | QEI_INTDIR | QEI_INTTIMER |
QEI_INTINDEX));
 QEIConfigure(QEI0_BASE, (QEI_CONFIG_CAPTURE_A_B | QEI_CONFIG_NO_RESET
| QEI_CONFIG_QUADRATURE | QEI_CONFIG_NO_SWAP), 900);
 SysCtlDelay(10);
//wait until it is ready
 QEIVelocityConfigure(QEI0_BASE, QEI_VELDIV_16, 40000);
//40000 is the period at which the velocity will be measured
 SysCtlDelay(10);
//wait until it is ready
 QEIPositionSet(QEI0_BASE, 0);
 SysCtlDelay(10);
//wait until it is ready
 QEIEnable(QEI0_BASE);
 SysCtlDelay(10);
//wait until it is ready
 QEIVelocityEnable(QEI0_BASE);
 SysCtlDelay(10);




PWMClockSet(PWM0_BASE,PWM_SYSCLK_DIV_64);
GPIOPinConfigure(GPIO_PF2_M0PWM2);
GPIOPinTypePWM(GPIO_PORTF_BASE, GPIO_PIN_2);
GPIOPinConfigure(GPIO_PF3_M0PWM3);
GPIOPinTypePWM(GPIO_PORTF_BASE, GPIO_PIN_3);


ui32PWMClock = ui32SysClkFreq / 64;
ui32Load = (ui32PWMClock / PWM_FREQUENCY) - 1;
PWMGenConfigure(PWM0_BASE, PWM_GEN_1, PWM_GEN_MODE_DOWN);
PWMGenPeriodSet(PWM0_BASE, PWM_GEN_1, ui32Load);
PWMPulseWidthSet(PWM0_BASE, PWM_OUT_2, ui32Load/2);
PWMOutputState(PWM0_BASE, PWM_OUT_2_BIT, true);
PWMPulseWidthSet(PWM0_BASE, PWM_OUT_3, ui32Load/2);
PWMOutputState(PWM0_BASE, PWM_OUT_3_BIT, true);
PWMGenEnable(PWM0_BASE, PWM_GEN_1);
ui32Index = 0;





//value data setup
int val[3];
int index=0;
int j;
for (j=0; j<3; j++) val[j]=0;



//timer setup
ui32Period = ui32SysClkFreq/2000; //2KHz timer frequency
TimerLoadSet(TIMER0_BASE, TIMER_A, ui32Period -1);
IntEnable(INT_TIMER0A);
TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
IntMasterEnable();
TimerEnable(TIMER0_BASE, TIMER_A);



UARTCharPut(UART0_BASE, 'W');
UARTCharPut(UART0_BASE, 'E');
UARTCharPut(UART0_BASE, 'L');
UARTCharPut(UART0_BASE, 'C');
UARTCharPut(UART0_BASE, 'O');
UARTCharPut(UART0_BASE, 'M');
UARTCharPut(UART0_BASE, 'E');
UARTCharPut(UART0_BASE, '!');
UARTCharPut(UART0_BASE, 10);
UARTCharPut(UART0_BASE, 13);
//UARTsendint(1234567);

while(1)
{


    //uart control
        if (UARTCharsAvail(UART0_BASE))
        {
           char c = UARTCharGet(UART0_BASE);

           if (c == 'c' | c == 'C'){
               //clear the current value (sets to 0)
               int i;
               for (i=0; i<3; i++) val[i]=0;
               index=0;
                UARTCharPut(UART0_BASE, 'C');
                UARTCharPut(UART0_BASE, 'L');
                UARTCharPut(UART0_BASE, 'R');
                UARTCharPut(UART0_BASE, 10);
                UARTCharPut(UART0_BASE, 13);
           }

           if (c == 13)
           {
                int input = val[0]*100+val[1]*10+val[2]; //input value range: 0-100
                Setpoint = input*6; // map 0-100 to 0-1999 and multiple by 51 //changed!! original = ((input*1999)/100)*51;
                response_index=0;
                UARTCharPut(UART0_BASE, 'U');
                UARTCharPut(UART0_BASE, 'P');
                UARTCharPut(UART0_BASE, 'D');
                UARTCharPut(UART0_BASE, 10);
                UARTCharPut(UART0_BASE, 13);
           }
           if (c > 47 && c < 58)
               {
               //update value
                   val[index]=c-48;
                   index++;
                   int i=0;

                   if(val[0]*100+val[1]*10+val[2] > 100) {for (i=0; i<3; i++) val[i]=0; index=0;}
                   i=0;

                   if (index>2) index=0;

                   for (i=0; i<3; i++) UARTCharPut(UART0_BASE, val[i]+48);
                  UARTCharPut(UART0_BASE, 10);
                  UARTCharPut(UART0_BASE, 13);
               }
           }

}
}

void Timer0IntHandler(void)
{
    // Clear the timer interrupt
    TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);

   hbridge(); //change to softbridge for the lab motor controller!!
}

void softbridge(){
    /* Get direction (1 = forward, -1 = backward). */
     Direction = QEIDirectionGet(QEI0_BASE);
    /* Get velocity(counts per period specified) and multiply by
    direction so that it is signed.*/
     Velocity = QEIVelocityGet(QEI0_BASE)*Direction;
    /* Get absolute position in counts.*/
     Position = QEIPositionGet(QEI0_BASE);

     if (response_index<10000){
                 response[response_index]=Position;
                 response_index++;
                 }

                 /*

                 if (Position != LastPosition){
                         //LastPosition = Position;
                         //response[response_index]=Position;
                        //response_index++;
                         //if (response_index==9999) response_index=0;
                        UARTsendint(Position);
                         UARTCharPut(UART0_BASE, 10);
                        UARTCharPut(UART0_BASE, 13);
                         }
                 */

    /* PD Control Algorithm */
     error=Setpoint-Position;
     P_term=Kp*error;
     D_term=Kd*(-Velocity); //Using Velocity from encoder
     u=P_term+D_term; //Output Command
     if (u<0){
     u=-u;
     GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_2, 0);
     } else{
     GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_2, 4);
     }
     if (u>=UpLimit){
     u=UpLimit;
     }
     else if (u<=DownLimit){
     u=DownLimit;
     }
     PWMPulseWidthSet(PWM0_BASE, PWM_OUT_4, u/100*ui32Load);
    //map the PWM signal depending on the output command u
}

void hbridge(){
    /* Get direction (1 = forward, -1 = backward). */
              Direction = QEIDirectionGet(QEI0_BASE);
             /* Get velocity(counts per period specified) and multiply by
             direction so that it is signed.*/
              Velocity = QEIVelocityGet(QEI0_BASE)*Direction;
             /* Get absolute position in counts.*/
              Position = QEIPositionGet(QEI0_BASE);


              if (response_index<10000){
              response[response_index]=Position;
              response_index++;
              }

              /*

              if (Position != LastPosition){
                      //LastPosition = Position;
                      //response[response_index]=Position;
                     //response_index++;
                      //if (response_index==9999) response_index=0;
                     UARTsendint(Position);
                      UARTCharPut(UART0_BASE, 10);
                     UARTCharPut(UART0_BASE, 13);
                      }
              */

             /* PD Control Algorithm */
              error=Setpoint-Position;
              P_term=Kp*error;
              D_term=Kd*(-Velocity); //Using Velocity from encoder
              u=P_term+D_term; //Output Command

              if (u>=0){
              if (u>=UpLimit){
                      u=UpLimit;
                      }
                      else if (u<=DownLimit){
                      u=DownLimit;
                      }
              }
              else{
                u=-u;
                 if (u>=UpLimit){
                                  u=UpLimit;
                                  }
                                  else if (u<=DownLimit){
                                  u=DownLimit;
                                  }
                 u=-u;

              }


              if (u<0){
              u=-u;
              PWMPulseWidthSet(PWM0_BASE, PWM_OUT_3, u/100*ui32Load);
              PWMPulseWidthSet(PWM0_BASE, PWM_OUT_2, 1);

              } else{
                  PWMPulseWidthSet(PWM0_BASE, PWM_OUT_2, u/100*ui32Load);
                  PWMPulseWidthSet(PWM0_BASE, PWM_OUT_3, 1);
              }

}

void UARTsendint(int score){

    if (score<0) {
        score=-score;
        UARTCharPut(UART0_BASE,'-');
    }

      unsigned int div = 1;
      unsigned int digit_count = 1;
      while ( div <= score / 10 ) {
        digit_count++;
        div *= 10;
      }
      while ( digit_count > 0 ) {
          UARTCharPut(UART0_BASE, score/div+48);
        score %= div;
        div /= 10;
        digit_count--;
      }
}
